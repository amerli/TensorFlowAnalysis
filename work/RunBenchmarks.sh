python Lb2Dppi.py
python Lb2Dppi.py --nogpu
python Lb2Dppi.py --cache
python Lb2Dppi.py --nogpu --cache
python Lb2Dppi.py --nograd
python Lb2Dppi.py --nograd --nogpu
python Lb2Dppi.py --nograd --cache
python Lb2Dppi.py --nograd --nogpu --cache

python Lb2Dppi.py --100k
python Lb2Dppi.py --100k --nogpu
python Lb2Dppi.py --100k --cache
python Lb2Dppi.py --100k --nogpu --cache
python Lb2Dppi.py --100k --nograd
python Lb2Dppi.py --100k --nograd --nogpu
python Lb2Dppi.py --100k --nograd --cache
python Lb2Dppi.py --100k --nograd --nogpu --cache

python D2KsPiPi.py
python D2KsPiPi.py --nogpu
python D2KsPiPi.py --cache
python D2KsPiPi.py --nogpu --cache
python D2KsPiPi.py --nograd
python D2KsPiPi.py --nograd --nogpu
python D2KsPiPi.py --nograd --cache
python D2KsPiPi.py --nograd --nogpu --cache

python D2KsPiPi.py --1m 
python D2KsPiPi.py --1m --nogpu
python D2KsPiPi.py --1m --cache
python D2KsPiPi.py --1m --nogpu --cache
python D2KsPiPi.py --1m --nograd
python D2KsPiPi.py --1m --nograd --nogpu
python D2KsPiPi.py --1m --nograd --cache
python D2KsPiPi.py --1m --nograd --nogpu --cache


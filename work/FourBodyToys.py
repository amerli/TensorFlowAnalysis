import tensorflow as tf

import sys, os
sys.path.append("..")
os.environ["CUDA_VISIBLE_DEVICES"] = ""

from TensorFlowAnalysis import *

from ROOT import TFile

if __name__ == "__main__" : 

  mp  = 0.938
  mpi = 0.139
  mk  = 0.497
  mlb = 5.620

  phsp = FourBodyHelicityPhaseSpace(mp, mpi, mpi, mpi, mlb)
  uniform_sample = phsp.UniformSample(1000000)

  dd = Const(5.)
  dr = Const(1.5)

  mass   = Const(1.232)
  width  = Const(0.117)

  ### Start of model description

  def model(x) : 
    (pa1, pa2, pb1, pb2) = phsp.FinalStateMomenta(x)
    d = phsp.Density(x)
    ma12 = Mass(pa1 + pa2)
    mb12 = Mass(pb1 + pb2)
    ampl = BreitWignerLineShape(ma12**2, mass,  width, mp, mpi, mb12, mlb, dr, dd, 0, 0)
    pdf = Density(ampl)*d
    return pdf

  ### End of model description

  init = tf.global_variables_initializer()
  sess = tf.Session()
  sess.run(init)

  majorant = EstimateMaximum(sess, model(phsp.data_placeholder), phsp.data_placeholder, sess.run( phsp.UniformSample(1000000) ) )*1.1
  print "Maximum = ", majorant

  data = RunToyMC( sess, model(phsp.data_placeholder), phsp.data_placeholder, phsp, 1000000, majorant, chunk = 1000000)

  f = TFile.Open("toyresult.root", "RECREATE")
  FillNTuple("toy", data, ["m1", "m2", "cos1", "cos2", "phi" ] )
  f.Close()
